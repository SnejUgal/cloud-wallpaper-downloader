use async_std::{
    io::{stderr, stdin},
    prelude::*,
};
use grammers_client::{Client, Config};
use grammers_session::Session;
use grammers_tl_types::{
    enums::{upload::File, Document, InputWallPaper, WallPaper},
    functions::{account::GetWallPaper, upload::GetFile},
    types::{self, InputDocumentFileLocation, InputWallPaperSlug},
};
use std::{error::Error, path::PathBuf};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Arguments {
    slug: String,
    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,
    #[structopt(long, env = "TELEGRAM_API_ID", hide_env_values = true)]
    api_id: i32,
    #[structopt(long, env = "TELEGRAM_API_HASH", hide_env_values = true)]
    api_hash: String,
    #[structopt(long, env = "TELEGRAM_SESSION_PATH")]
    session_path: String,
}

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let Arguments {
        slug,
        output,
        api_id,
        api_hash,
        session_path,
    } = Arguments::from_args();
    let output = output.unwrap_or_else(|| format!("{}.jpg", slug).into());

    eprintln!("connecting...");
    let mut session = Session::load_or_create(&session_path).unwrap();
    let mut client = Client::connect(Config {
        session,
        api_id,
        api_hash: api_hash.clone(),
        params: Default::default(),
    })
    .await?;
    eprintln!("connected");

    authorize(&mut client, api_id, &api_hash).await?;

    let wallpaper = InputWallPaper::Slug(InputWallPaperSlug { slug });
    let wallpaper = client.invoke(&GetWallPaper { wallpaper }).await?;
    let wallpaper = match wallpaper {
        WallPaper::Paper(wallpaper) => wallpaper,
        _ => return Err("no wallpaper".into()),
    };

    let document = match wallpaper.document {
        Document::Document(document) => document,
        _ => return Err("no document".into()),
    };

    let wallpaper = download_document(&mut client, document).await?;
    async_std::fs::write(output, wallpaper).await?;

    Ok(())
}

async fn authorize(client: &mut Client, api_id: i32, api_hash: &str) -> Result<(), Box<dyn Error>> {
    if client.is_authorized().await? {
        return Ok(());
    }

    eprintln!("Please authorize (cloud password not supported):");

    let mut phone_number = String::with_capacity(12);
    eprint!("Enter phone number: ");
    stderr().flush().await?;
    stdin().read_line(&mut phone_number).await?;

    client
        .request_login_code(phone_number.trim(), api_id, &api_hash)
        .await?;

    let mut code = String::with_capacity(5);
    eprint!("Enter code: ");
    stderr().flush().await?;
    stdin().read_line(&mut code).await?;

    client.sign_in(&code).await.unwrap();
    eprintln!("Authorized!");

    Ok(())
}

async fn download_document(
    client: &mut Client,
    document: types::Document,
) -> Result<Vec<u8>, Box<dyn Error>> {
    const LIMIT: i32 = 1024 * 1024;
    let mut downloaded = Vec::new();

    let location = InputDocumentFileLocation {
        id: document.id,
        access_hash: document.access_hash,
        file_reference: document.file_reference,
        thumb_size: String::new(),
    };
    let mut request = GetFile {
        precise: true,
        cdn_supported: false,
        location: location.into(),
        offset: 0,
        limit: LIMIT,
    };

    while downloaded.len() < document.size as usize {
        let file = match client.invoke(&request).await? {
            File::File(file) => file,
            File::CdnRedirect(..) => return Err("cdn not supported".into()),
        };

        downloaded.extend_from_slice(&file.bytes);
        request.offset += file.bytes.len() as i32;
    }

    Ok(downloaded)
}
